Имя проекта postgres2020-19890905
===============
Разворачиваем и настраиваем БД с большими данными

Цель: знать различные механизмы загрузки данных, уметь пользоваться различными механизмами загрузки данных.

Необходимо провести сравнение скорости работы запросов на различных СУБД.
1. Выбрать одну из СУБД
2. Загрузить в неё данные (10 Гб)
3. Сравнить скорость выполнения запросов на PosgreSQL и выбранной СУБД
4. Описать что и как делали и с какими проблемами столкнулись

| Описание | Команда |
| ------ | ------ |
| Создал ВМ postgres типа e2-standart-4 |  |
| Развернул postgres 13 | sudo apt update && sudo apt upgrade -y && sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list' && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - && sudo apt-get update && sudo apt-get -y install postgresql && sudo apt install unzip |
| Создал Storage taxi_postgres и экспортировал в него данные из BigQuery chicago_taxi_trips |  |
| Скопировал данные из Storage на сервер в tmp | gsutil -m cp -R gs://taxi_postgres . |
| Создал БД taxi | create database taxi; |
| Создал таблицу taxi_trips | create table taxi_trips (unique_key text, taxi_id text, trip_start_timestamp TIMESTAMP, trip_end_timestamp TIMESTAMP, trip_seconds bigint, trip_miles numeric, pickup_census_tract bigint, dropoff_census_tract bigint, pickup_community_area bigint, dropoff_community_area bigint, fare numeric, tips numeric, tolls numeric, extras numeric, trip_total numeric, payment_type text, company text, pickup_latitude numeric, pickup_longitude numeric, pickup_location text, dropoff_latitude numeric, dropoff_longitude numeric, dropoff_location text); |
| Загрузил данные в таблицу taxi_trips | COPY taxi_trips(unique_key, taxi_id, trip_start_timestamp, trip_end_timestamp, trip_seconds, trip_miles, pickup_census_tract, dropoff_census_tract, pickup_community_area, dropoff_community_area, fare, tips, tolls, extras, trip_total, payment_type, company, pickup_latitude, pickup_longitude, pickup_location, dropoff_latitude, dropoff_longitude, dropoff_location)FROM PROGRAM 'awk FNR-1 /tmp/taxi_postgres/*.csv | cat' DELIMITER ',' CSV HEADER; |
| Посмотрел размер таблицы taxi_trips **(14Гб)** | SELECT pg_size_pretty( pg_relation_size( 'taxi_trips' ) ); |
| Выполнил select | SELECT payment_type, round(sum(tips)/sum(trip_total)*100, 0) as tips_percent, count(*) as c FROM taxi_trips group by payment_type order by 3 |
| Время выполнения с дефолтными настройками **255313.938 ms (04:15.314)** |  |
| Изменил настройки и перезапустил postgres | max_connections = 20; shared_buffers = 4GB; effective_cache_size = 12GB; maintenance_work_mem = 1GB; checkpoint_completion_target = 0.9; wal_buffers = 16MB; default_statistics_target = 100; random_page_cost = 1.1; effective_io_concurrency = 200; work_mem = 104857kB; min_wal_size = 2GB; max_wal_size = 8GB; max_worker_processes = 4; max_parallel_workers_per_gather = 2; max_parallel_workers = 4; max_parallel_maintenance_workers = 2 |
| Время выполнения запроса уменьшилось **13265.123 ms (00:13.265)** | Количество строк в таблице taxi_trips **34105663** |
| Создал ВМ mysql типа e2-standart-4 |  |
| Установил mysql | sudo apt install mysql-server |
| Создал БД taxi | create database taxi; |
| Создал таблицу taxi_trips | Аналогично postgresql |
| Загрузил данные в таблицу taxi_trips | LOAD DATA INFILE '/var/lib/mysql-files/taxi_postgres/taxi_postgres_000000000001.csv' INTO TABLE taxi_trips FIELDS TERMINATED BY ',' IGNORE 1 ROWS; |
| Выполнил select | Аналогично postgresql |
| Время выполнения **17.82 sec)** | Количество строк в таблице taxi_trips **6480168** |

**Особых проблем при выполнении ДЗ не было. Возникли сложности с загрузкой данных в БД mysql, загружал по одному файлу csv. В таблице postgresql 34105663 строк, запрос выполнялся 13.265 секунд. В таблице mysql 6480168 строк, запрос выполнялся 17.82 секунд. Из полученных данных можно сделать вывод, что postgresql более подходит для работы с большими данными, чем mysql. Наверное можно оптимизировать настройки mysql, не работал ранее с этой СУБД**