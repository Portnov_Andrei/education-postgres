Имя проекта postgres2020-19890905, ВМ postgres
==========
1 вариант:
------
1. создайте новый кластер PostgresSQL 13 (на выбор - GCE, CloudSQL)
2. зайдите в созданный кластер под пользователем postgres **sudo -u postgres psql**
3. создайте новую базу данных testdb **create database testdb;**
4. зайдите в созданную базу данных под пользователем postgres **\c testdb;**
5. создайте новую схему testnm **create schema testnm;**
6. создайте новую таблицу t1 с одной колонкой c1 типа integer **create table t1(c1 integer);**
7. вставьте строку со значением c1=1 **insert into t1 values(1);**
8. создайте новую роль readonly **create role readonly;**
9. дайте новой роли право на подключение к базе данных testdb **grant connect on database testdb TO readonly;**
10. дайте новой роли право на использование схемы testnm **grant usage on schema testnm to readonly;**
11. дайте новой роли право на select для всех таблиц схемы testnm **grant select on all tables in schema testnm TO readonly;**
12. создайте пользователя testread с паролем test123 **create role testread with password 'test123';**
13. дайте роль readonly пользователю testread **grant readonly TO testread;**
14. зайдите под пользователем testread в базу данных testdb **\c testdb testread; Нужно дать пользователю testread право на login: alter user testread LOGIN; Также нужно в pg_hba.conf изменить peer аутентификацию на md5. После изменения pg_hba.conf нужно перезапустить кластер: sudo pg_ctlcluster 13 main restart**
15. сделайте select * from t1;
16. получилось? **Не получилось**
17. напишите что именно произошло в тексте домашнего задания **Создали БД testdb. В этой БД создали схему testnm. Создали таблицу t1, она создалась в схеме public. Создали новую роль readonly. Роли readonly дали права на подключение к БД testdb, на использование схемы testnm, на select для всех таблиц схемы testnm. Создали новую роль testread с паролем test123. Дали роль readonly роли testread, т.е. роль testread унаследовала все права readonly.**
18. у вас есть идеи почему? ведь права то дали? **Мы дали права роли testread только на select для всех таблиц схемы testnm**
19. посмотрите на список таблиц **Таблица t1 создана в схеме public**
20. подсказка в шпаргалке под пунктом 20
21. а почему так получилось с таблицей **Потому, что в search_path указано "$user", public. Схемы $user нет, поэтому таблица по умолчанию создалась в public**
22. вернитесь в базу данных testdb под пользователем postgres **\c testdb postgres**
23. удалите таблицу t1 **drop table t1;**
24. создайте ее заново но уже с явным указанием имени схемы testnm **create table testnm.t1(c1 integer);**
25. вставьте строку со значением c1=1 **insert into testnm.t1 values(1);**
26. зайдите под пользователем testread в базу данных testdb **\c testdb testread;**
27. сделайте select * from testnm.t1;
28. получилось? **Нет**
29. есть идеи почему? **Посмотрел шпаргалку. Потому что grant select on all tables in schema testnm TO readonly дал доступ только для существующих на тот момент времени таблиц, а t1 пересоздавалась**
30. как сделать так чтобы такое больше не повторялось? **Определить права доступа по умолчанию: alter default privileges in schema testnm grant select on tables to readonly;**
31. сделайте select * from testnm.t1;
32. получилось? **Нет**
33. есть идеи почему? **Посмотрел шпаргалку. Потому что alter default будет действовать для новых таблиц, а grant select on all tables in schema testnm TO readonly отработал только для существующих на тот момент времени. надо сделать снова или grant select или пересоздать таблицу**
34. сделайте select * from testnm.t1;
35. получилось? **Да**
36. ура!
37. теперь попробуйте выполнить команду create table t2(c1 integer); insert into t2 values (2);
38. а как так? нам же никто прав на создание таблиц и insert в них под ролью readonly не давал?
39. есть идеи? **Посмотрел шпагралку. Это все потому что search_path указывает в первую очередь на схему public. А схема public создается в каждой базе данных по умолчанию. И grant на все действия в этой схеме дается роли public. А роль public добавляется всем новым пользователям. Соответсвенно каждый пользователь может по умолчанию создавать объекты в схеме public любой базы данных, ес-но если у него есть право на подключение к этой базе данных. Чтобы раз и навсегда забыть про роль public - а в продакшн базе данных про нее лучше забыть - выполните следующие действия \c testdb postgres; revoke create on schema public from public; revoke all on database testdb from public; \c testdb testread;**
40. если вы справились сами то расскажите что сделали и почему, если смотрели шпаргалку - объясните что сделали и почему выполнив указанные в ней команды **Команда revoke create on schema public from public лишает прав на создание схемы public роль public. Команда revoke all on database testdb from public лишает всех прав в БД testdb роль public**
41. теперь попробуйте выполнить команду create table t3(c1 integer); insert into t2 values (2);
42. расскажите что получилось и почему **Создать таблицу t3 не получилось, т.к. мы забрали у public все права. Вставить данные получилось, потому что revoke all on database testdb from public будет действовать для новых таблиц, а доступ роли public давался при создании таблицы.**