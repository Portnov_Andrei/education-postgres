Имя проекта postgres2020-19890905
=========
1 вариант:
----------

- Создал виртуальную машину **postgres** c Ubuntu 20.04 LTS в GCE типа e2-medium в default VPC в зоне us-central1-a
- Поставил на нее PostgreSQL через **sudo apt install postgresql**
- Проверил, что кластер запущен через sudo -u postgres pg_lsclusters
- Зашел из под пользователя postgres в psql и сделал произвольную таблицу с произвольным содержимым
postgres=# create table test(c1 text);
postgres=# insert into test values('1');
\q
- Остановил postgres. Например, через sudo -u postgres pg_ctlcluster 12 test stop
- Создал новый standard persistent диск GKE через Compute Engine -> Disks в том же регионе и зоне что GCE инстанс размером 10GB
- Добавил свеже-созданный диск к виртуальной машине - зашел в режим ее редактирования и выбрал пункт attach existing disk
- Проинициализировал диск согласно инструкции и подмонтировал файловую систему, нужно поменять имя диска на актуальное, в нашем случае /dev/sdb -
https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux
- Сделал пользователя postgres владельцем /mnt/data - chown -R postgres:postgres /mnt/data/
- Перенес содержимое /var/lib/postgres/12 в /mnt/data - mv /var/lib/postgresql/12 /mnt/data
- Попытался запустить кластер - sudo -u postgres pg_ctlcluster 12 test start
- Напишите получилось или нет и почему. **Не получилось. Возникла ошибка: Error: /var/lib/postgresql/12/test is not accessible or does not exist. Ошибка возникла потому, что мы перенесли /var/lib/postgres/12 в /mnt/data, а в конфигурационных файлах изменения не вносили.**
- Задание: найти конфигурационный параметр в файлах раположенных в /etc/postgresql/12/test, который надо поменять и поменять его.
- Напишите, что и почему поменяли **Поменял параметр data_directory в файле postgresql.conf, т.к. он отвечает за размещение файлов БД**
- Попытался запустить кластер - sudo -u postgres pg_ctlcluster 12 test start
- Напишите получилось или нет и почему. **Получилось, потому что внес изменения в файл postgresql.conf**
- Зашел через psql и проверил содержимое ранее созданной таблицы **Данные на месте**

2 вариант:
---------

- Создал виртуальную машину **docker** в GCE с Ubuntu 20.04
- Поставил на нем Docker Engine **Ставил, как показывали на лекции**
- Сделал каталог /var/lib/postgres **sudo mkdir /var/lib/postgres**
- Развернул контейнер с PostgreSQL 13 смонтировав в него /var/lib/postgres **sudo  docker run --name pg-server --network pg-net -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v /var/lib/postgres:/var/lib/postgresql/data postgres:13**
- Развернул контейнер с клиентом postgres **sudo docker run -it --rm --network pg-net --name pg-client postgres:13 psql -h pg-server -U postgres**
- Подключился из контейнера с клиентом к контейнеру с сервером и сделал таблицу
- Подключится к контейнеру с сервером с ноутбука **psql -h 35.188.119.151 -U postgres**
- Удалил контейнер с сервером **Останавил контейнер: sudo docker stop 4ab459e87c84. Удалил контейнер: sudo docker rm 4ab459e87c84**
- Создал его заново **Без открытия порта наружу: sudo  docker run --name pg-server --network pg-net -e POSTGRES_PASSWORD=postgres -d -v /var/lib/postgres:/var/lib/postgresql/data postgres:13**
- Подключился снова из контейнера с клиентом к контейнеру с сервером **sudo docker run -it --rm --network pg-net --name pg-client postgres:13 psql -h pg-server -U postgres**
- Проверил, что данные остались на месте **Данные остались на месте, т.к. при развертывании контейнера мы смонтировали в него /var/lib/postgres. Т.е. данные хранятся не в самом контейнере, иначе бы они исчезли**