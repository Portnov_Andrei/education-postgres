Имя проекта postgres2020-19890905
===============
**1 ВМ - postgres, 2 ВМ - postgres2, 3 ВМ - postgres3**
- На 1 ВМ создаем таблицы test для записи, test2 для запросов на чтение.
- Создаем публикацию таблицы test и подписываемся на публикацию таблицы test2 с ВМ №2. **CREATE PUBLICATION test_pub FOR TABLE test;
CREATE SUBSCRIPTION test2_sub 
CONNECTION 'host=34.122.238.197 port=5432 user=postgres password= dbname=postgres' 
PUBLICATION test2_pub WITH (copy_data = false);**
- На 2 ВМ создаем таблицы test2 для записи, test для запросов на чтение.
- Создаем публикацию таблицы test2 и подписываемся на публикацию таблицы test с ВМ №1. **CREATE PUBLICATION test2_pub FOR TABLE test2;
CREATE SUBSCRIPTION test_sub 
CONNECTION 'host=34.69.19.150 port=5432 user=postgres password= dbname=postgres' 
PUBLICATION test_pub WITH (copy_data = false);**
- 3 ВМ использовать как реплику для чтения и бэкапов (подписаться на таблицы из ВМ №1 и №2 ). **CREATE SUBSCRIPTION test_sub2 
CONNECTION 'host=34.69.19.150 port=5432 user=postgres password= dbname=postgres' 
PUBLICATION test_pub WITH (copy_data = true);
CREATE SUBSCRIPTION test2_sub2 
CONNECTION 'host=34.122.238.197 port=5432 user=postgres password= dbname=postgres' 
PUBLICATION test2_pub WITH (copy_data = true);**
- Небольшое описание, того, что получилось. **Настроил логическую репликацию таблицы test с 1 ВМ на 2 ВМ. Настроил логическую репликацию таблицы test2 с 2 ВМ на 1 ВМ. Настроил логическую репликацию таблицы test с 1 ВМ и таблицы test2 с 2 ВМ на 3 ВМ**