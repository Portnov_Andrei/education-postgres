Имя проекта postgres2020-19890905, ВМ postgres
==============

1. Настройте сервер так, чтобы в журнал сообщений сбрасывалась информация о блокировках, удерживаемых более 200 миллисекунд. Воспроизведите ситуацию, при которой в журнале появятся такие сообщения. **В postgresql.conf устанавливаем deadlock_timeout = 200ms, log_lock_waits = on.
В журнале сообщение выглядят так:**
- rmgr: Storage     len (rec/tot):     42/    42, tx:          0, lsn: 0/01606B98, prev 0/01606B70, desc: CREATE base/16384/16393
- rmgr: Standby     len (rec/tot):     42/    42, tx:        493, lsn: 0/01606BC8, prev 0/01606B98, desc: LOCK xid 493 db 16384 rel 16393
2. Смоделируйте ситуацию обновления одной и той же строки тремя командами UPDATE в разных сеансах. Изучите возникшие блокировки в представлении pg_locks и убедитесь, что все они понятны. Пришлите список блокировок и объясните, что значит каждая.

1 UPDATE

|  locktype    | relation | virtxid | xid |       mode       | granted|
---------------|----------|---------|-----|------------------|--------|
|relation      | accounts |         |     | RowExclusiveLock | t **блокировка Exclusive на изменяемую строку в accounts**|
|relation      | pg_locks |         |     | AccessShareLock  | t **блокировка Share на просмотр pg_locks**|
|virtualxid    |          | 4/144   |     | ExclusiveLock    | t **блокировка Exclusive на virtualxid просмотра pg_locks**|
|transactionid |          |         | 500 | ExclusiveLock    | t **блокировка Exclusive на update строки в accounts**|

2 UPDATE

|  locktype    | relation | virtxid | xid |       mode       | granted|
|--------------|----------|---------|-----|------------------|---------
|relation      | accounts |         |     | RowExclusiveLock | t **блокировка Exclusive на изменяемую строку в accounts**|
|relation      | pg_locks |         |     | AccessShareLock  | t **блокировка Share на просмотр pg_locks**|
|virtualxid    |          | 5/12    |     | ExclusiveLock    | t **блокировка Exclusive на virtualxid просмотра pg_locks**|
|tuple         | accounts |         |     | ExclusiveLock    | t **у 1 UPDATE создался tuple, он блокирован Exclusive 2 UPDATE**|
|transactionid |          |         | 500 | ShareLock        | f **доступ Share к строке блокируется 1 UPDATE**|
|transactionid |          |         | 501 | ExclusiveLock    | t **блокировка Exclusive текущей транзакции на update строки в accounts**|

3 UPDATE

|  locktype    | relation | virtxid | xid |       mode       | granted|
|--------------|----------|---------|-----|------------------|---------|
|relation      | accounts |         |     | RowExclusiveLock | t **блокировка Exclusive на accounts**|
|relation      | pg_locks |         |     | AccessShareLock  | t **блокировка Share на pg_locks**|
|virtualxid    |          | 6/6     |     | ExclusiveLock    | t **блокировка Exclusive на virtualxid**|
|transactionid |          |         | 502 | ExclusiveLock    | t **блокировка Exclusive текущей транзакции на update строки в accounts**|
|tuple         | accounts |         |     | ExclusiveLock    | f **доступ Exclusive к tuple блокируется 2 UPDATE**|

3. Воспроизведите взаимоблокировку трех транзакций. Можно ли разобраться в ситуации постфактум, изучая журнал сообщений? **Разобраться можно. Ниже пример журнала по блокировкам из п.2**
   - rmgr: Transaction len (rec/tot):     34/    34, tx:        499, lsn: 0/0163ACE8, prev 0/0163ACB0, desc: COMMIT 2020-11-27 06:19:50.181476 UTC`
   - rmgr: Standby     len (rec/tot):     50/    50, tx:          0, lsn: 0/0163AD10, prev 0/0163ACE8, desc: RUNNING_XACTS nextXid 500 latestCompletedXid 499 oldestRunningXid 500
   - rmgr: Heap        len (rec/tot):     71/    71, tx:        500, lsn: 0/0163AD48, prev 0/0163AD10, desc: HOT_UPDATE off 1 xmax 500 flags 0x20 ; new off 4 xmax 0, blkref #0: rel 1663/16384/16394 blk 0
   - rmgr: Standby     len (rec/tot):     54/    54, tx:          0, lsn: 0/0163AD90, prev 0/0163AD48, desc: RUNNING_XACTS nextXid 501 latestCompletedXid 499 oldestRunningXid 500; 1 xacts: 500
   - rmgr: Standby     len (rec/tot):     62/    62, tx:          0, lsn: 0/0163ADC8, prev 0/0163AD90, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 499 oldestRunningXid 500; 3 xacts: 502 501 500
   - rmgr: XLOG        len (rec/tot):    114/   114, tx:          0, lsn: 0/0163AE08, prev 0/0163ADC8, desc: CHECKPOINT_ONLINE redo 0/163ADC8; tli 1; prev tli 1; fpw true; xid 0:503; oid 24576; multi 1; offset 0; oldest xid 478 in DB 1; oldest multi 1 in DB 1; oldest/newest commit timestamp xid: 0/0; oldest running xid 500; online
   - rmgr: Standby     len (rec/tot):     62/    62, tx:          0, lsn: 0/0163AE80, prev 0/0163AE08, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 499 oldestRunningXid 500; 3 xacts: 502 501 500
   - rmgr: Transaction len (rec/tot):     34/    34, tx:        500, lsn: 0/0163AEC0, prev 0/0163AE80, desc: COMMIT 2020-11-27 07:21:50.560848 UTC
   - rmgr: Heap        len (rec/tot):     59/   259, tx:        501, lsn: 0/0163AEE8, prev 0/0163AEC0, desc: LOCK off 4: xid 501: flags 0x00 LOCK_ONLY EXCL_LOCK , blkref #0: rel 1663/16384/16394 blk 0 FPW
   - rmgr: Heap        len (rec/tot):     70/    70, tx:        501, lsn: 0/0163AFF0, prev 0/0163AEE8, desc: HOT_UPDATE off 4 xmax 501 flags 0x20 ; new off 5 xmax 501, blkref #0: rel 1663/16384/16394 blk 0
   - rmgr: Standby     len (rec/tot):     58/    58, tx:          0, lsn: 0/0163B038, prev 0/0163AFF0, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 500 oldestRunningXid 501; 2 xacts: 502 501
   - rmgr: Transaction len (rec/tot):     34/    34, tx:        501, lsn: 0/0163B078, prev 0/0163B038, desc: COMMIT 2020-11-27 07:23:52.412665 UTC
   - rmgr: Heap        len (rec/tot):     54/    54, tx:        502, lsn: 0/0163B0A0, prev 0/0163B078, desc: LOCK off 5: xid 502: flags 0x00 LOCK_ONLY EXCL_LOCK , blkref #0: rel 1663/16384/16394 blk 0
   - rmgr: Heap        len (rec/tot):     70/    70, tx:        502, lsn: 0/0163B0D8, prev 0/0163B0A0, desc: HOT_UPDATE off 5 xmax 502 flags 0x20 ; new off 6 xmax 502, blkref #0: rel 1663/16384/16394 blk 0
   - rmgr: Standby     len (rec/tot):     54/    54, tx:          0, lsn: 0/0163B120, prev 0/0163B0D8, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 501 oldestRunningXid 502; 1 xacts: 502
   - rmgr: Transaction len (rec/tot):     34/    34, tx:        502, lsn: 0/0163B158, prev 0/0163B120, desc: COMMIT 2020-11-27 07:24:01.611796 UTC
   - rmgr: Standby     len (rec/tot):     50/    50, tx:          0, lsn: 0/0163B180, prev 0/0163B158, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 502 oldestRunningXid 503
   - rmgr: Standby     len (rec/tot):     50/    50, tx:          0, lsn: 0/0163B1B8, prev 0/0163B180, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 502 oldestRunningXid 503
   - rmgr: XLOG        len (rec/tot):    114/   114, tx:          0, lsn: 0/0163B1F0, prev 0/0163B1B8, desc: CHECKPOINT_ONLINE redo 0/163B1B8; tli 1; prev tli 1; fpw true; xid 0:503; oid 24576; multi 1; offset 0; oldest xid 478 in DB 1; oldest multi 1 in DB 1; oldest/newest commit timestamp xid: 0/0; oldest running xid 503; online
   - rmgr: Standby     len (rec/tot):     50/    50, tx:          0, lsn: 0/0163B268, prev 0/0163B1F0, desc: RUNNING_XACTS nextXid 503 latestCompletedXid 502 oldestRunningXid 503

4. Могут ли две транзакции, выполняющие единственную команду UPDATE одной и той же таблицы (без where), заблокировать друг друга? **Да, могут заблокировать друг друга**
* Попробуйте воспроизвести такую ситуацию.
**Запустил 2 сесии. В одной выполнил UPDATE accounts SET amount = 2006, в другой UPDATE accounts SET amount = 2007**
**Данные из pg_locks:**

1 UPDATE

|  locktype    | relation | virtxid | xid |       mode       | granted|
|--------------|----------|---------|-----|------------------|---------|
|relation      | 16394    |         |     | RowExclusiveLock | t|
|relation      | pg_locks |         |     | AccessShareLock  | t|
|virtualxid    |          | 4/148   |     | ExclusiveLock    | t|
|transactionid |          |         | 504 | ExclusiveLock    | t|

2 UPDATE

|   locktype   | relation | virtxid | xid |       mode       | granted|
|--------------|----------|---------|-----|------------------|---------|
|relation      | 16394    |         |     | RowExclusiveLock | t|
|relation      | pg_locks |         |     | AccessShareLock  | t|
|virtualxid    |          | 5/13    |     | ExclusiveLock    | t|
|tuple         | 16394    |         |     | ExclusiveLock    | t|
|transactionid |          |         | 504 | ShareLock        | f|
|transactionid |          |         | 505 | ExclusiveLock    | t|

